# OpenVPN bootstrap (v2) 

This tool helps in configuring OpenVPN server on fresh Ubuntu server (tested on Ubuntu 16.04).

### Features
* Fully automatic OpenVPN setup, building server keys and configuration file, adding UFW rules and sysctl configuration.
* User management with just one command line tool (creating, revoking, generating client config files)

### Who shoulnd't use it
This tool is not for you if:
* You are not intended to use VPN as a "proxy"
* You use `iptables` instead of `ufw` (this tool automatically installs `ufw` if it is not found in the system)
* You are afraid of using command line in order to communicate with this tool.

### Installation and usage
* First of all you need Python 3.6 installed. Instructions can be found [here](https://askubuntu.com/questions/865554/how-do-i-install-python-3-6-using-apt-get).
* Clone this repository somewhere, for example to your home directory: 
`git clone https://bitbucket.org/a_kazakov/openvpn-bootstrap.git`
* Within the repository run `./vpn --help` to get information about all available commands
* Run `./vpn setup` to generate and edit configuration file
* Run `./vpn init` to configure VPN server
* Run `./vpn add <client_name>` to add new client (configuration `.ovpn` file will be stored to `configs` folder)
* Run `./vpn list` to list clients
* Run `./vpn revoke <client_name>` to revoke client's certificate
* Run `./vpn gen` to re-generate all clients configuration

### How does it work
* This tool uses `easy_rsa` for  managing keys and certificates
* When running `./vpn init`, the following steps are taken:
	* EasyRSA latest version is downloaded from GitHub
	* EasyRSA PKI gets initialized
	* CA certificate, server certificate with key, DH key pair, TLS auth secret and CRL are generated
	* Server configuration file is built and stored at `/etc/openvpn/server.conf`
	* UFW is being configured:
		* `DEFAULT_FORWARD_POLICY="ACCEPT"` -> **`/etc/default/ufw`**
		* `*nat`
		`:POSTROUTING ACCEPT [0:0]`
		`-A POSTROUTING -s <vpn_net> -o <adapter> -j MASQUERADE`
		`COMMIT` -> **`/etc/ufw/before.rules`**
		* VPN port is opened (`ufw allow <vpn_port>/<protocol>`)
	* `net.ipv4.ip_forward=1` and `net.ipv6.conf.all.forwarding=1` are added to **`/etc/sysctl.conf`**
    * OpenVPN service is restarted `service openvpn restart`
 * When running  `./vpn add <username>`:
	 * A new client certificate is generated via `easy_rsa`
	 * Client configuration `.ovpn` file is generated and stored in `configs` folder. All needed certificates and keys are embedded into `.ovpn` file so there is no need to transfer any other files (such as `ca.crt` or `client.crt/client.key`) to client's machine.
