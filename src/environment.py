import shutil
from argparse import ArgumentParser, Namespace
from typing import Any, Callable, Dict, List, Optional, Tuple, Type, TypeVar

from clients import Client
from config_files import ClientConfigurationFile, ServerConfigurationFile
from dependencies import DependencyManager
from settings import Settings
from side_configs import SysCtlForwardConfig, UfwBeforeRules, UfwMainConfig
from storage import StorageManager
from utils import Singleton, launch_editor, yes_or_no_prompt


T = TypeVar("T")


class EnvironmentMeta(Singleton):
    def __new__(
        clstype: Type,
        name: str,
        bases: Tuple[Type, ...],
        attrs: Dict[str, Any],
    ) -> Type:
        attrs["methods"] = [
            (name, method)
            for name, method in attrs.items()
            if name.startswith("run")
        ]
        return type.__new__(clstype, name, bases, attrs)


def command(help_text: str, args: Dict[str, str] = None) -> Callable[[T], T]:
    def decorator(func: T) -> T:
        setattr(func, "help", help_text)
        setattr(func, "args_help", args or {})
        return func
    return decorator


class Environment(metaclass=EnvironmentMeta):
    methods: List[Tuple[str, object]]

    def check_settings_inited(self) -> None:
        if not Settings().ready:
            raise RuntimeError("Settings are not initialized. Run 'vpn setup'.")

    @command("Generate config file template, launch editor and validate it.")
    def run_setup(self) -> None:
        Settings().generate_file()
        while True:
            launch_editor(Settings.FILENAME)
            try:
                Settings().reload()
                if not Settings().ready:
                    raise ValueError("Some fields of settings are not set.")
            except ValueError as ex:
                print(str(ex))
                if yes_or_no_prompt(f"Do you want to open editor again?"):
                    continue
            break

    @command(
        (
            "Create server configuration and certificates, configure firewall "
            "and traffic forwarding."
        ),
        args={
            "rewrite": "If set, recreates existing server keys and certificates.",
        }
    )
    def run_init(self, rewrite: bool = False) -> None:
        self.check_settings_inited()
        StorageManager().create_all(rewrite)
        ServerConfigurationFile().build()
        UfwMainConfig().enable()
        UfwBeforeRules().enable()
        DependencyManager().ufw.allow(
            Settings().user_port,
            Settings().transport,
        )
        SysCtlForwardConfig().enable()
        DependencyManager().openvpn.restart()

    @command("Build (or rebuild) server configuration file.")
    def run_gen_server_config(self) -> None:
        self.check_settings_inited()
        ServerConfigurationFile().build()
        DependencyManager().openvpn.restart()

    @command("Confirure firewall rules. Changes should be applied manually.")
    def run_configure_ufw(self) -> None:
        self.check_settings_inited()
        UfwMainConfig().enable()
        UfwBeforeRules().enable()
        DependencyManager().ufw.allow(
            Settings().user_port,
            Settings().transport,
        )
        DependencyManager().openvpn.restart()

    @command("Modifies sysctl config to allow traffic forwarding.")
    def run_configure_sysctl(self) -> None:
        SysCtlForwardConfig().enable()
        DependencyManager().openvpn.restart()

    @command(
        "Clean server configuration and certificates, discard firewall rules "
        "and traffic forwarding."
    )
    def run_purge(self) -> None:
        if not yes_or_no_prompt(
            "This will delete all user certificates and system configuration "
            "created by this tool. Are you sure want to continue?"
        ):
            return
        shutil.rmtree(Settings().easy_rsa_path)
        shutil.rmtree(Settings().storage_path)
        shutil.rmtree(Settings().client_configs_path)
        UfwMainConfig().disable()
        UfwBeforeRules().disable()
        SysCtlForwardConfig().disable()
        DependencyManager().openvpn.stop()

    @command(
        "Create new client and generate config.",
        args={
            "name": "Client name to be created",
        }
    )
    def run_add(self, name: str) -> None:
        client = Client(name, exist_required=False)
        client.create()
        ClientConfigurationFile(client).build()

    @command(
        "Regenerate client configs.",
    )
    def run_gen(self) -> None:
        for client in Client.all():
            if not client.revoked:
                ClientConfigurationFile(client).build()

    @command(
        "Revoke client's certificate.",
        args={
            "name": "Client name to be revoked",
        }
    )
    def run_revoke(self, name: str) -> None:
        if not yes_or_no_prompt(
            f"Are you sure want to revoke certificate for {name}? "
            f"This can't be undone."
        ):
            raise RuntimeError("Aborted by user")
        Client(name).revoke()

    @command(
        "List all clients of this vpn server.",
    )
    def run_list(self) -> None:
        all_clients = Client.all()
        active_clients = list(filter(lambda c: not c.revoked, all_clients))
        revoked_clients = list(filter(lambda c: c.revoked, all_clients))
        if active_clients:
            print("Active clients:")
            for client in active_clients:
                print(f" - {client.name}")
        else:
            print("There are no active VPN clients.")
        print()
        if revoked_clients:
            print("Revoked clients:")
            for client in revoked_clients:
                print(f" - {client.name}")
        else:
            print("There are no revoked VPN clients.")
        print()

    def run(self, args: Namespace) -> None:
        runner: Optional[Callable] = getattr(
            self,
            f"run_{args.cmd}",
            None,
        )
        if runner is None:
            raise RuntimeError(f"Unable to run {args.cmd} command")
        kwargs = {
            name: getattr(args, name)
            for name in runner.__annotations__
            if name not in ("return", "self", )
        }
        runner(**kwargs)

    def configure_argparser(self, parser: ArgumentParser) -> None:
        all_subparsers = parser.add_subparsers(
            dest="cmd",
            help="Action to do",
        )
        for name, method in self.methods:
            if not name.startswith("run_"):
                continue
            cmd = name[4:]
            subparser = all_subparsers.add_parser(
                cmd,
                help=getattr(method, "help", ""),
            )
            for arg, arg_type in method.__annotations__.items():
                if arg not in ("return", "self", ):
                    if issubclass(arg_type, bool):
                        subparser.add_argument(
                            f"--{arg}",
                            action="store_true",
                            help=getattr(method, "args_help", {}).get(arg),
                        )
                    elif issubclass(arg_type, str):
                        subparser.add_argument(
                            arg,
                            help=getattr(method, "args_help", {}).get(arg),
                        )
                    else:
                        raise RuntimeError(f"Type {arg_type} is not supported.")
