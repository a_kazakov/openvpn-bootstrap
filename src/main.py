#!/usr/bin/env python3.6

import sys
from argparse import ArgumentParser

from environment import Environment


def main() -> None:
    try:
        env = Environment()
        parser = ArgumentParser()
        env.configure_argparser(parser)
        args = parser.parse_args()
        if args.cmd is None:
            raise RuntimeError(
                f"Command should be specified. Run {sys.argv[0]} --help for "
                f"details"
            )
        env.run(args)
    except (ValueError, RuntimeError) as ex:
        print(str(ex))
        sys.exit(1)


if __name__ == "__main__":
    main()
