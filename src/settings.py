import os
import re
from configparser import ConfigParser
from typing import (
    Any,
    Callable,
    Dict,
    Generic,
    List,
    Optional,
    Set,
    Tuple,
    Type,
    TypeVar,
)

from utils import Singleton


T = TypeVar("T")


class SettingsField(Generic[T]):
    def __init__(
        self,
        value_type: Type[T],
        default: Any = None,
        *,
        hidden: bool = False,
        validator: Optional[Callable[[T], None]] = None,
        postprocessor: Optional[Callable[[T], T]] = None,
    ) -> None:
        self.name: str = ""
        self.value_type = value_type
        self.default = default
        self.hidden = hidden
        self.validator = validator
        self.postprocessor = postprocessor

    def init(self, name: str) -> None:
        self.name = name
        if self.default is not None:
            if not isinstance(self.default, self.value_type):
                raise ValueError(
                    f"Default value for {name} must be of type {self.value_type}. "
                    f"Got {self.default} of type {type(self.default)}."
                )

    def __set__(self, inst: Optional["Settings"], value: T) -> None:
        if not isinstance(value, self.value_type):
            raise ValueError(
                f"Values for {self.name} must be of type {self.value_type}. "
                f"Got {value} of type {type(value)}."
            )
        if self.validator is not None:
            self.validator(value)
        if self.postprocessor is not None:
            value = self.postprocessor(value)
        inst.__dict__[self.name] = value

    def __get__(
        self,
        inst: Optional["Settings"],
        type_: Optional[Type["Settings"]],
    ) -> T:
        try:
            return inst.__dict__[self.name]
        except KeyError:
            raise AttributeError

    def __delete__(
        self,
        inst: Optional["Settings"],
    ) -> None:
        try:
            del inst.__dict__[self.name]
        except KeyError:
            raise AttributeError


class SettingsMeta(Singleton):
    def __new__(
        clstype: Type,
        name: str,
        bases: Tuple[Type, ...],
        attrs: Dict[str, Any],
    ) -> Type:
        fields: List[SettingsField] = []
        for key, field in attrs.items():
            if isinstance(field, SettingsField):
                field.init(key)
                fields.append(field)
        attrs = {"fields": fields, **attrs}
        result = super().__new__(clstype, name, bases, attrs)
        return result


def ip_to_number(ip: str) -> int:
    ip_num = 0
    for octet in map(int, ip.split(".")):
        ip_num = (ip_num << 8) | octet
    return ip_num


def number_to_ip(number: int) -> str:
    return ".".join(map(
        str,
        ((number >> (8 * x)) & 0xff for x in range(3, -1, -1))
    ))


def parse_net_definition(definition: str) -> Tuple[str, int]:
    spl = definition.split("/")
    if len(spl) != 2:
        raise ValueError
    ip, str_prefix = spl
    return ip, int(str_prefix)


def validate_ip(ip: str) -> None:
    if not re.match(r"^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}$", ip):
        raise ValueError(f"{ip} doesn't look like a valid IP address.")
    if not all(0 <= x <= 255 for x in map(int, ip.split("."))):
        raise ValueError(f"{ip} doesn't look like a valid IP address.")


def validate_hosname(host: str) -> None:
    RE = (
        r"^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*"
        r"([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$"
    )
    if not re.match(RE, host):
        raise ValueError(f"{host} doesn't look like a valid host name.")


def validate_net(net: str) -> None:
    try:
        ip, prefix = parse_net_definition(net)
        validate_ip(ip)
        if not (1 <= prefix <= 31):
            raise ValueError
        ip_num = ip_to_number(ip)
        suffix = 32 - prefix
        if ip_num & ((1 << suffix) - 1) != 0:
            raise ValueError
    except ValueError:
        raise ValueError(f"{net} doesn't look like a valid network definition.")


def pp_path(path: str) -> str:
    return os.path.abspath(os.path.expanduser(path))


class Settings(metaclass=SettingsMeta):
    fields: List[SettingsField]

    FILENAME = os.path.abspath("./settings.ini")

    server_name = SettingsField(str)

    use_tap = SettingsField(bool, False)
    use_tcp = SettingsField(bool, False)
    server_hostname = SettingsField(str, validator=validate_hosname)
    listen_port = SettingsField(int, 1194)
    user_port = SettingsField(int, 1194)
    dns1 = SettingsField(str, "8.8.8.8", validator=validate_ip)
    dns2 = SettingsField(str, "8.8.4.4", validator=validate_ip)

    vpn_net = SettingsField(str, "10.8.0.0/24", validator=validate_net)
    key_size = SettingsField(int, 2048)

    easy_rsa_path = SettingsField(str, "./easy_rsa", postprocessor=pp_path)
    storage_path = SettingsField(str, "./storage", postprocessor=pp_path)
    templates_path = SettingsField(str, "./templates", postprocessor=pp_path)
    client_configs_path = SettingsField(str, "./configs", postprocessor=pp_path)

    external_config_prelude = SettingsField(str, "OpenVPN config begin")
    external_config_postlude = SettingsField(str, "OpenVPN config end")

    def __init__(self) -> None:
        self.reload()

    def reload(self) -> None:
        self.__default_fields: Set[str] = set()
        file_settings = ConfigParser()
        file_settings.read(self.FILENAME)
        try:
            file_section: Dict[str, str] = dict(file_settings["vpn"])
        except KeyError:
            file_section = {}
        try:
            for field in self.fields:
                if hasattr(self, field.name):
                    delattr(self, field.name)
                raw_value = file_section.get(field.name)
                if raw_value is None and field.default is not None:
                    raw_value = field.default
                    self.__default_fields.add(field.name)
                if (
                    isinstance(raw_value, str) and
                    not isinstance(field.value_type, str)
                ):
                    if raw_value == "":
                        continue
                    value = (
                        field.value_type(raw_value)
                        if field.value_type is not bool
                        else field.value_type.lower() in ("yes", "true", "1", "on",)
                    )
                else:
                    value = raw_value
                if value is not None:
                    setattr(self, field.name, value)
        except ValueError as ex:
            raise ValueError(
                f"Unable to load settings: {ex}"
            )

    def generate_file(self) -> None:
        with open("settings.ini", "wt", encoding="utf-8") as f:
            f.write("[vpn]\n")
            name_size = max(len(field.name) for field in self.fields)
            for field in self.fields:
                aligned_field_name = f"{{:<{name_size}}}".format(field.name)
                value = getattr(self, field.name, "")
                if type(value) is bool:
                    value = ("yes" if value else "no")
                if field.name not in self.__default_fields:
                    f.write(f"{aligned_field_name}   = {value}\n")
                else:
                    f.write(f"; {aligned_field_name} = {value}\n")

    @property
    def vpn_net_ip(self) -> str:
        return parse_net_definition(self.vpn_net)[0]

    @property
    def vpn_net_prefix(self) -> int:
        return parse_net_definition(self.vpn_net)[1]

    @property
    def vpn_net_mask(self) -> str:
        prefix = parse_net_definition(self.vpn_net)[1]
        suffix = 32 - prefix
        res_num = ((1 << prefix) - 1) << suffix
        return number_to_ip(res_num)

    @property
    def transport(self) -> str:
        return "tcp" if self.use_tcp else "udp"

    @property
    def device(self) -> str:
        return "tap" if self.use_tap else "tun"

    @property
    def ready(self) -> bool:
        for field in self.fields:
            if not hasattr(self, field.name):
                return False
        return True
