import os
import subprocess as sp
import tempfile
from typing import Optional, Tuple

from dependencies import DependencyManager
from settings import Settings
from utils import Singleton, find_default_adapter


class SideConfig:
    def __init__(
        self,
        path: str,
        *,
        comment_prefix: str = "#",
        use_sudo: bool = True,
    ) -> None:
        self.path = path
        self.comment_prefix = comment_prefix
        self.use_sudo = use_sudo

    def add_block(self, user_text: str) -> None:
        if self.find_block() is not None:
            self.remove_block()
        ins_pos = self._insert_position
        config_text = self._read()
        if ins_pos is None:
            head = config_text
            tail = ""
        else:
            head = config_text[:ins_pos]
            tail = config_text[ins_pos:]
        head += f"\n{self._prelude}\n"
        head += user_text
        head += f"\n{self._postlude}\n"
        self._write(head + tail)

    def find_block(self) -> Optional[str]:
        config_text = self._read()
        begin_position, end_position = self._find_block_bounds(
            config_text=config_text,
            outer=False,
        )
        if begin_position is None or end_position is None:
            return None
        return config_text[begin_position:end_position]

    def remove_block(self) -> None:
        config_text = self._read()
        begin_position, end_position = self._find_block_bounds(
            config_text=config_text,
            outer=True,
        )
        if begin_position is None or end_position is None:
            return
        config_text = config_text[:begin_position] + config_text[end_position:]
        self._write(config_text)

    @property
    def _prelude(self) -> str:
        return f"{self.comment_prefix} {Settings().external_config_prelude}"

    @property
    def _postlude(self) -> str:
        return f"{self.comment_prefix} {Settings().external_config_postlude}"

    @property
    def _insert_position(self) -> Optional[int]:
        return None

    def _read(self) -> str:
        if not self.use_sudo:
            with open(self.path, "rt", encoding="utf-8") as f:
                return f.read()
        with tempfile.TemporaryDirectory() as tmp_dir:
            tmp_file = os.path.join(tmp_dir, "file")
            with open(tmp_file, "wt"):
                pass  # Touch the file
            retcode = sp.call([
                "sudo",
                "cp",
                "--no-preserve=mode,ownership",
                self.path,
                tmp_file,
            ])
            if retcode != 0:
                raise RuntimeError("Failed to read configuration")
            with open(tmp_file, "rt", encoding="utf-8") as f:
                return f.read()

    def _write(self, text: str) -> None:
        if not self.use_sudo:
            with open(self.path, "wt", encoding="utf-8") as f:
                f.write(text)
            return
        with tempfile.TemporaryDirectory() as tmp_dir:
            tmp_file = os.path.join(tmp_dir, "file")
            with open(tmp_file, "wt", encoding="utf-8") as f:
                f.write(text)
            retcode = sp.call([
                "sudo",
                "cp",
                "--no-preserve=mode,ownership",
                tmp_file,
                self.path,
            ])
            if retcode != 0:
                raise RuntimeError("Failed to save configuration")

    def _find_block_bounds(
        self,
        config_text,
        *,
        outer: bool = False,
    ) -> Tuple[Optional[int], Optional[int]]:
        try:
            begin_position = config_text.index(self._prelude)
            end_position = config_text.index(self._postlude)
        except ValueError:
            return None, None
        if outer:
            begin_position -= 1
            end_position += len(self._postlude) + 1
        else:
            begin_position += len(self._prelude) + 1
            end_position -= 1
        return begin_position, end_position


class SysCtlForwardConfig(SideConfig, metaclass=Singleton):
    def __init__(self) -> None:
        super().__init__("/etc/sysctl.conf")

    def reload(self) -> None:
        sp.call(["sudo", "sysctl", "-p"])

    def enable(self) -> None:
        self.add_block("\n".join((
            "net.ipv4.ip_forward=1",
            "net.ipv6.conf.all.forwarding=1",
        )))
        self.reload()

    def disable(self) -> None:
        self.remove_block()
        self.reload()


class UfwMainConfig(SideConfig, metaclass=Singleton):
    def __init__(self) -> None:
        super().__init__(DependencyManager().ufw.main_config_path)

    def reload(self) -> None:
        print("Ufw must be enabled manually.")

    def enable(self) -> None:
        self.add_block("DEFAULT_FORWARD_POLICY=\"ACCEPT\"")
        self.reload()

    def disable(self) -> None:
        self.remove_block()
        self.reload()


class UfwBeforeRules(SideConfig, metaclass=Singleton):
    def __init__(self) -> None:
        super().__init__(DependencyManager().ufw.before_rules_path)

    def reload(self) -> None:
        print("Ufw must be enabled manually.")

    def enable(self) -> None:
        adapter = find_default_adapter()
        self.add_block("\n".join((
            f"*nat",
            f":POSTROUTING ACCEPT [0:0]",
            f"-A POSTROUTING -s {Settings().vpn_net} -o {adapter} -j MASQUERADE",
            f"COMMIT",
        )))
        self.reload()

    def disable(self) -> None:
        self.remove_block()
        self.reload()

    @property
    def _insert_position(self) -> Optional[int]:
        config_text = self._read()
        try:
            return config_text.index("*filter")
        except ValueError:
            return None
