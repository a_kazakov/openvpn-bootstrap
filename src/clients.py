import os
import re
from typing import List, Set

from storage import StorageManager
from dependencies import DependencyManager
from utils import memoize_no_args


@memoize_no_args
def get_revoked_certificates() -> Set[str]:
    revoked_info = DependencyManager().openssl.get_revoked_info(
        StorageManager().crl.path,
    )
    return {
        match.group(1).replace(":", "").upper()
        for match in re.finditer(r"Serial Number:\s+(\S+)", revoked_info)
    }


class Client:
    def __init__(self, name: str, exist_required=True) -> None:
        self.name = name
        if not self.exists and exist_required:
            raise RuntimeError(f"Client {name} doesn't exist.")

    def create(self, exist_ok=False) -> None:
        if self.exists:
            if exist_ok:
                return
            raise RuntimeError(f"Client {self.name} already exists.")
        DependencyManager().easy_rsa.call(
            "build-client-full",
            self.name,
            "nopass",
        )

    @property
    def cert_path(self) -> str:
        return os.path.abspath(
            os.path.join(
                DependencyManager().easy_rsa.certs_path,
                f"{self.name}.crt",
            )
        )

    @property
    def key_path(self) -> str:
        return os.path.abspath(
            os.path.join(
                DependencyManager().easy_rsa.keys_path,
                f"{self.name}.key",
            )
        )

    def read_cert(self) -> str:
        with open(self.cert_path, "rt") as f:
            return f.read()

    def read_key(self) -> str:
        with open(self.key_path, "rt") as f:
            return f.read()

    @property
    def exists(self) -> bool:
        return os.path.exists(self.cert_path) and os.path.exists(self.key_path)

    @property
    def cert_serial(self) -> str:
        cert_info = DependencyManager().openssl.get_cert_info(self.cert_path)
        match = re.search(r"Serial Number:\s+(\S+)", cert_info)
        if match is None:
            raise RuntimeError(
                f"Unable to parse certificate for client {self.name}.",
            )
        return match.group(1).replace(":", "").upper()

    @property
    def revoked(self) -> bool:
        return self.cert_serial in get_revoked_certificates()

    def revoke(self) -> None:
        if self.revoked:
            raise RuntimeError(
                f"Certificate for {self.name} is already revoked."
            )
        DependencyManager().easy_rsa.call("revoke", self.name)
        StorageManager().create_crl(rewrite=True)

    @classmethod
    def all(cls) -> List["Client"]:
        result: List["Client"] = []
        for filename in os.listdir(DependencyManager().easy_rsa.certs_path):
            if filename == "server.crt":
                continue
            if not filename.endswith(".crt"):
                continue
            result.append(Client(filename[:-4]))
        return sorted(result, key=lambda x: x.name)
