import os
import subprocess as sp
import tempfile
from typing import Any

from clients import Client
from dependencies import DependencyManager
from settings import Settings
from storage import StorageManager
from utils import Singleton


def load_template(temaplate_file: str, **kwargs: Any) -> str:
    with open(temaplate_file, "rt", encoding="utf-8") as f:
        return f.read().format(**kwargs)


class ServerConfigurationFile(metaclass=Singleton):
    def build(self) -> None:
        text = load_template(
            os.path.join(Settings().templates_path, "server.conf"),
            transport=Settings().transport,
            device=Settings().device,
            port=Settings().listen_port,
            ca_cert_path=StorageManager().ca.path,
            server_cert_path=StorageManager().server_cert.path,
            server_key_path=StorageManager().server_key.path,
            tls_auth_path=StorageManager().tls_auth.path,
            crl_path=StorageManager().crl.path,
            dh_path=StorageManager().dh.path,
            vpn_net_ip=Settings().vpn_net_ip,
            vpn_net_mask=Settings().vpn_net_mask,
            dns1=Settings().dns1,
            dns2=Settings().dns2,
        )
        with tempfile.TemporaryDirectory() as tmp_dir:
            tmp_file = os.path.join(tmp_dir, "server.conf")
            with open(tmp_file, "wt", encoding="utf-8") as f:
                f.write(text)
            final_path = DependencyManager().openvpn.config_file_path
            retcode = sp.call(["sudo", "mv", tmp_file, final_path])
            if retcode != 0:
                raise RuntimeError("Unable to save server configuration")


class ClientConfigurationFile():
    def __init__(self, client: Client) -> None:
        self.client = client

    def build(self) -> None:
        text = load_template(
            os.path.join(Settings().templates_path, "client.conf"),
            transport=Settings().transport,
            device=Settings().device,
            hostname=Settings().server_hostname,
            port=Settings().listen_port,
            ca_cert=StorageManager().ca.read(),
            client_cert=self.client.read_cert(),
            client_key=self.client.read_key(),
            tls_auth=StorageManager().tls_auth.read(),
        )
        os.makedirs(Settings().client_configs_path, exist_ok=True)
        dest_path = os.path.join(
            Settings().client_configs_path,
            f"{Settings().server_name}_{self.client.name}.ovpn"
        )
        with open(dest_path, "wt", encoding="utf-8") as f:
            f.write(text)
