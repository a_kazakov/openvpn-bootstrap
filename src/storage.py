import os
from typing import Generator

from settings import Settings
from dependencies import DependencyManager
from utils import Singleton


class StorageFile:
    def __init__(self, local_path: str) -> None:
        self.local_path = local_path
        self.path = os.path.abspath(
            os.path.join(Settings().storage_path, self.local_path),
        )
        os.makedirs(Settings().storage_path, exist_ok=True)

    @property
    def exists(self) -> bool:
        return os.path.exists(self.path)

    def read(self) -> str:
        with open(self.path, "rt", encoding="utf-8") as f:
            return f.read()


class StorageManager(metaclass=Singleton):
    def __init__(self) -> None:
        if not os.path.exists(Settings().storage_path):
            DependencyManager().easy_rsa.call("init-pki")
        self.ca = StorageFile("ca.crt")
        self.server_cert = StorageFile("issued/server.crt")
        self.server_key = StorageFile("private/server.key")
        self.dh = StorageFile("dh.pem")
        self.crl = StorageFile("crl.pem")
        self.tls_auth = StorageFile("secret.key")

    @property
    def fields(self) -> Generator[StorageFile, None, None]:
        for value in self.__dict__.values():
            if isinstance(value, StorageFile):
                yield value

    def create_ca(self, rewrite: bool) -> None:
        if self.ca.exists:
            return
        print("Creating CA certificate...")
        DependencyManager().easy_rsa.call("build-ca", "nopass")

    def create_server_certificate(self, rewrite: bool) -> None:
        if self.server_cert.exists and self.server_key.exists:
            return
        print("Creating server certificates...")
        DependencyManager().easy_rsa.call("build-server-full", "server", "nopass")

    def create_dh(self, rewrite: bool) -> None:
        if self.dh.exists and not rewrite:
            return
        print("Creating DH pair...")
        DependencyManager().easy_rsa.call("gen-dh")

    def create_crl(self, rewrite: bool) -> None:
        if self.crl.exists and not rewrite:
            return
        print("Creating CRL...")
        DependencyManager().easy_rsa.call("gen-crl")

    def create_tls_auth(self, rewrite: bool) -> None:
        if self.tls_auth.exists and not rewrite:
            return
        print("Creating secret key...")
        DependencyManager().openvpn.call(
            "--genkey",
            "--secret",
            self.tls_auth.path,
        )

    def create_all(self, rewrite: bool) -> None:
        self.create_ca(rewrite)
        self.create_server_certificate(rewrite)
        self.create_dh(rewrite)
        self.create_crl(rewrite)
        self.create_tls_auth(rewrite)
