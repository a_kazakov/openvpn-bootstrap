import os
import shutil
import subprocess as sp
import tempfile
from typing import Tuple, TypeVar, Type

from settings import Settings
from utils import Singleton, which


class BaseDependency:
    def check(self) -> bool:
        raise NotImplementedError

    def install(self) -> None:
        raise NotImplementedError


class BaseApplicationDependency(BaseDependency):
    @property
    def binary_name(self) -> str:
        raise NotImplementedError

    @property
    def package_name(self) -> str:
        return self.binary_name

    def check(self) -> bool:
        return which(self.binary_name) is not None

    def install(self) -> None:
        retcode = sp.call([
            "sudo",
            "apt-get",
            "install",
            "-y",
            self.package_name,
        ])
        if retcode != 0:
            raise RuntimeError(f"Unable to install {self.package_name}")

    def call(self, *args: str, sudo: bool = False) -> None:
        cmd_prefix: Tuple[str, ...] = tuple(["sudo"] if sudo else [])
        cmd = (
            cmd_prefix +
            (self.binary_name, ) +
            args
        )
        retcode = sp.call(cmd)
        if retcode != 0:
            cmd_text = " ".join(cmd)
            raise RuntimeError(f"Command '{cmd_text}' failed")

    def get_call_output(self, *args: str, sudo: bool = False) -> str:
        cmd_prefix: Tuple[str, ...] = tuple(["sudo"] if sudo else [])
        cmd = (
            cmd_prefix +
            (self.binary_name, ) +
            args
        )
        p = sp.Popen(cmd, stdout=sp.PIPE)
        out, _err = p.communicate()
        if p.returncode != 0:
            cmd_text = " ".join(cmd)
            raise RuntimeError(f"Command '{cmd_text}' failed")
        return out.decode()


class OpenVpnDependency(BaseApplicationDependency):
    @property
    def binary_name(self) -> str:
        return "openvpn"

    @property
    def config_path(self) -> str:
        return "/etc/openvpn"

    @property
    def config_file_path(self) -> str:
        return os.path.join(self.config_path, "server.conf")

    def control(self, cmd: str) -> None:
        retcode = sp.call(["sudo", "service", "openvpn", cmd])
        if retcode != 0:
            raise RuntimeError(f"Failed to {cmd} OpenVPN")

    def start(self) -> None:
        self.control("start")

    def stop(self) -> None:
        self.control("stop")

    def restart(self) -> None:
        self.control("restart")


class EasyRsaDependency(BaseDependency):
    def check(self) -> bool:
        return os.path.exists(os.path.join(
            Settings().easy_rsa_path,
            "easyrsa3"
        ))

    def install(self) -> None:
        if os.path.exists(Settings().easy_rsa_path):
            shutil.rmtree(Settings().easy_rsa_path)
        with tempfile.TemporaryDirectory() as tmp_dir:
            zip_path = os.path.join(tmp_dir, "master.zip")
            DependencyManager().wget.call(
                "https://github.com/OpenVPN/easy-rsa/archive/master.zip",
                "-O", zip_path,
            )
            DependencyManager().unzip.call("-q", zip_path, "-d", tmp_dir)
            shutil.copytree(
                os.path.join(tmp_dir, "easy-rsa-master"),
                Settings().easy_rsa_path,
            )

    def call(self, *args: str) -> None:
        DependencyManager().openssl  # Ensure it is installed
        binary = os.path.join(
            Settings().easy_rsa_path,
            "easyrsa3/easyrsa",
        )
        p = sp.Popen(
            [binary, *args],
            env={
                "EASYRSA_PKI": self.pki_path,
                "EASYRSA_KEY_SIZE": str(Settings().key_size),
            }
        )
        p.wait()
        if p.returncode != 0:
            cmd_text = " ".join(*args)
            raise RuntimeError(f"Command 'easyrsa {cmd_text}' failed")

    @property
    def pki_path(self) -> str:
        return os.path.join(Settings().storage_path)

    @property
    def certs_path(self) -> str:
        return os.path.join(self.pki_path, "issued")

    @property
    def keys_path(self) -> str:
        return os.path.join(self.pki_path, "private")


class UnzipDependency(BaseApplicationDependency):
    @property
    def binary_name(self) -> str:
        return "unzip"


class OpenSslDependency(BaseApplicationDependency):
    @property
    def binary_name(self) -> str:
        return "openssl"

    def get_cert_info(self, cert_path: str) -> str:
        return self.get_call_output("x509", "-text", "-in", cert_path)

    def get_revoked_info(self, crl_path: str) -> str:
        return self.get_call_output("crl", "-text", "-in", crl_path)


class UfwDependency(BaseApplicationDependency):
    @property
    def binary_name(self) -> str:
        return "ufw"

    @property
    def main_config_path(self) -> str:
        return "/etc/default/ufw"

    @property
    def before_rules_path(self) -> str:
        return "/etc/ufw/before.rules"

    def allow(self, port: int, transport: str) -> None:
        self.call("allow", f"{port}/{transport}", sudo=True)


class WgetDependency(BaseApplicationDependency):
    @property
    def binary_name(self) -> str:
        return "wget"


DepType = TypeVar("DepType", bound=BaseDependency)


class DependencyManager(metaclass=Singleton):
    @property
    def openvpn(self) -> OpenVpnDependency:
        return self._get_dependency("openvpn", OpenVpnDependency)

    @property
    def easy_rsa(self) -> EasyRsaDependency:
        return self._get_dependency("easy_rsa", EasyRsaDependency)

    @property
    def unzip(self) -> UnzipDependency:
        return self._get_dependency("unzip", UnzipDependency)

    @property
    def openssl(self) -> OpenSslDependency:
        return self._get_dependency("openssl", OpenSslDependency)

    @property
    def ufw(self) -> UfwDependency:
        return self._get_dependency("ufw", UfwDependency)

    @property
    def wget(self) -> WgetDependency:
        return self._get_dependency("wget", WgetDependency)

    def _get_dependency(self, name: str, type_: Type[DepType]) -> DepType:
        attr_name = f"_{name}"
        if hasattr(self, attr_name):
            return getattr(self, attr_name)
        result = type_()
        if not result.check():
            print(f"Dependency {name} not found. Installing {name}...")
            result.install()
        setattr(self, attr_name, result)
        return result
