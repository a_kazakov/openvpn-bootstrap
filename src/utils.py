import os
import subprocess as sp

from typing import Callable, Type, TypeVar, Dict, Optional


T = TypeVar("T")


class Singleton(type):
    _instances: Dict[Type, object] = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            # cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]


def memoize_no_args(func: Callable[[], T]) -> Callable[[], T]:
    cache: Dict[int, T] = {}

    def inner() -> T:
        if 1 not in cache:
            cache[1] = func()
        return cache[1]

    return inner


def which(binary: str) -> Optional[str]:
    paths = os.environ['PATH'].split(os.pathsep)
    for path in paths:
        possible_full_path = os.path.join(path, binary)
        if os.path.exists(possible_full_path):
            return possible_full_path
    return None


def launch_editor(path: str) -> None:
    editor = os.getenv("EDITOR")
    if editor is None:
        raise RuntimeError(
            f"Unable to find default editor. Please edit {path} yourself."
        )
    retcode = sp.call([editor, path])
    if retcode != 0:
        raise RuntimeError(
            f"Unable to launch {editor}. Please edit {path} yourself."
        )


def yes_or_no_prompt(prompt: str) -> bool:
    while True:
        response = input(f"{prompt} [y/n] ")
        if response.lower() in ("yes", "y",):
            return True
        if response.lower() in ("no", "n",):
            return False


def find_default_adapter() -> str:
    p = sp.Popen(["ip", "route"], stdout=sp.PIPE)
    out, err_ = p.communicate()
    if p.returncode != 0:
        raise RuntimeError("Unable to read IP routes.")
    lines = [l.strip() for l in out.decode().strip().split("\n")]
    try:
        default_line = next(l for l in lines if l.startswith("default "))
        words = default_line.split()
        idx = words.index("dev") + 1
        return words[idx]
    except (StopIteration, ValueError, IndexError):
        raise RuntimeError("Unable to find default IP route.")
